//console.log("Hello World!");

let num1 = +prompt(`Enter a number:`);

console.log(`The number entered is ${num1}`);

//step4
for(let ctr = num1; ctr >= 0; ctr--){

	// console.log(ctr);

	//step5
	if(ctr <= 50){
		console.log(`The current value is at 50. Terminating the loop.`);
		break;
	}

	//step6
	if(ctr % 10 == 0){
		console.log(`The number is divisible by 10. Skipping the number.`);
		continue;
	}

	//step7
	if(ctr % 5 == 0){
		console.log(ctr);
	}

} //for loop end


//step8
let strWord = `supercalifragilisticexpialidocious`;
console.log(strWord);
// console.log(`word length: ${strWord.length}`);

//step9
let strConsonants = ``;

//step10
	for(let ctr = 0;ctr <= (strWord.length-1); ctr++){

		// console.log(`ctr value: ${ctr}`);
		// console.log(strWord[ctr]);

		if(strWord[ctr] == `a` || strWord[ctr] == `e` || strWord[ctr] == `i` || strWord[ctr] == `o` || strWord[ctr] == `u`){
			// console.log(`skip ${strWord[ctr]}`);
			continue;
		}else{
			strConsonants += strWord[ctr];
			// console.log(`add consonant ${strWord[ctr]}`);
			// console.log(strConsonants);
		}
	}

console.log(strConsonants);
